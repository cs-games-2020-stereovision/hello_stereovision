#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main() {
    Mat left = imread("../tsukuba_l.png", CV_LOAD_IMAGE_GRAYSCALE);
    Mat right = imread("../tsukuba_r.png", CV_LOAD_IMAGE_GRAYSCALE);

    Mat disparity, disparity8u, depth;
    Ptr<StereoBM> stereo = StereoBM::create(16,15);
    stereo->compute(left, right, disparity);
    disparity.convertTo(disparity8u, CV_8U);

    imshow("Disparity map", disparity8u);
    imwrite("../out_disparity.png", disparity8u);
    waitKey();
    return 0;
}
